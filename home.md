**NOTE:** This is librsvg's development wiki.  You may want to go to the [end-user wiki](https://wiki.gnome.org/Projects/LibRsvg) instead; that one has links to the API documentation and resources for people who just want to use the library.

* [Internals documentation](https://gnome.pages.gitlab.gnome.org/librsvg/internals/rsvg/index.html)

* [SVG2 Features](svg2-features)

* [SVG2 Text Features](text-features)